package com.example.mycellproject

import com.google.gson.annotations.SerializedName


data class Packets(
    @SerializedName("Success")
    val Success: Boolean?,
    @SerializedName("ErrorMessage")
    val ErrorMessage: String?,
    @SerializedName("Result")
    val Result: Result?
)

data class Result(
    @SerializedName("PacketTitle")
    val PacketTitle: String?,
    @SerializedName("PacketSubTitle")
    val PacketSubTitle: String?,
    @SerializedName("PacketList")
    val PacketList: List<PacketList>?
)
data class PacketList(
    @SerializedName("Title")
    val Title: String?,
    @SerializedName("AppSize")
    val AppSize: Int?,
    @SerializedName("Gb")
    val Gb: Int?,
    @SerializedName("Dk")
    val Dk: Int?,
    @SerializedName("Sms")
    val Sms: Int?,
    @SerializedName("Mins")
    val Mins: Int?,
    @SerializedName("Price")
    val Price: Int?,
    @SerializedName("Currency")
    val Currency: String?,
    @SerializedName("StartColor")
    val StartColor: String?,
    @SerializedName("EndColor")
    val EndColor: String?
)