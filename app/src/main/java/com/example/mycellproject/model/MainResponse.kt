package com.example.mycellproject.model

import com.google.gson.annotations.SerializedName

data class MainResponse(
    @SerializedName("Success")
    val Success: Boolean?,
    @SerializedName("ErrorMessage")
    val ErrorMessage: String?,
    @SerializedName("Result")
    val Result: Result?

)

data class Result(
    @SerializedName("UserInfo")
    val UserInfo: UserInfo?,
    @SerializedName("UserModuleList")
    val UserModuleList: List<Module>?

)
data class UserInfo(
    @SerializedName("UserId")
    val UserId: String?,
    @SerializedName("UserName")
    val UserName: String?,
    @SerializedName("UserImageUrl")
    val UserImageUrl: String?
)
data class Module(
    @SerializedName("ModuleName")
    val ModuleName : String?
)


