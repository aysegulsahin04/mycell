package com.example.mycellproject.viewmodel

import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mycellproject.model.MainResponse
import com.example.mycellproject.model.Module
import com.example.mycellproject.model.UserInfo
import com.example.mycellproject.service.MainResposeAPIService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class FirstScreenViewModel: ViewModel() {
    private val mainApiService = MainResposeAPIService()
    private val disposable = CompositeDisposable()
    val module= MutableLiveData<List<Module>>()
    val userInfo = MutableLiveData<UserInfo>()
    val mainResponse = MutableLiveData<MainResponse>()

    fun getMainData(){
        getDataFromApi()
    }

    private fun getDataFromApi(){
        disposable.add(
            mainApiService.getData()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<MainResponse>(){
                    override fun onSuccess(t: MainResponse) {
                        t.Result.let {
                        }
                        module.value = t.Result?.UserModuleList
                        userInfo.value = t.Result?.UserInfo
                        mainResponse.value = t
                    }
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }

                })
        )
    }
}