package com.example.mycellproject.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mycellproject.PacketList
import com.example.mycellproject.Packets
import com.example.mycellproject.Result
import com.example.mycellproject.model.MainResponse
import com.example.mycellproject.service.PacketAPIService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class SecondScreenViewModel: ViewModel() {
    private val packetApiService = PacketAPIService()
    private val disposable = CompositeDisposable()
    val packetList = MutableLiveData<List<PacketList>>()
    val result = MutableLiveData<Result>()
    val packet = MutableLiveData<Packets>()

    fun getPacketData(){
        getDataFromApi()
    }

    private fun getDataFromApi(){
        disposable.add(
            packetApiService.getData()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Packets>(){
                    override fun onSuccess(t: Packets) {
                        packetList.value = t.Result?.PacketList
                        result.value = t.Result
                        packet.value = t
                    }
                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }
                })
        )

    }
}