package com.example.mycellproject.service

import com.example.mycellproject.Packets
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class PacketAPIService {
    private val BASE_URL = "http://ahmtkocamn.xyz/aysegul/"
    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(PacketAPI::class.java)

    fun getData(): Single<Packets> {
        return api.getPacketList()


    }
}