package com.example.mycellproject.service

import com.example.mycellproject.model.MainResponse
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class MainResposeAPIService {
    private val BASE_URL = "http://ahmtkocamn.xyz/aysegul/"

    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(MainResponseAPI::class.java)

    fun getData(): Single<MainResponse>{
        return api.getData()
    }

}
