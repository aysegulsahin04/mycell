package com.example.mycellproject.service
import com.example.mycellproject.Packets
import com.example.mycellproject.model.MainResponse
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET

interface MainResponseAPI {
    @GET("ekran_1.json")

    fun getData():Single<MainResponse>
}