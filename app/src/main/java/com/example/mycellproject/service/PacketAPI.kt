package com.example.mycellproject.service

import com.example.mycellproject.Packets
import io.reactivex.Single
import retrofit2.http.GET

interface PacketAPI {
    @GET("ekran_2.json")

    fun getPacketList(): Single<Packets>
}