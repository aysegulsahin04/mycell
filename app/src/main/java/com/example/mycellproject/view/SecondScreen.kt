package com.example.mycellproject.view

import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.mycellproject.Adapter.PacketListAdapter
import com.example.mycellproject.R
import com.example.mycellproject.viewmodel.SecondScreenViewModel
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_second_screen.*

class SecondScreen : Fragment() {
    private lateinit var viewModel : SecondScreenViewModel
    private val packetAdapter = PacketListAdapter(arrayListOf())
    var isSuccess: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_second_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SecondScreenViewModel::class.java)
        viewModel.getPacketData()

        packetList.layoutManager = LinearLayoutManager(context)
        packetList.adapter = packetAdapter

        observeLiveData()
    }

    fun observeLiveData(){
        viewModel.packetList.observe(viewLifecycleOwner, Observer {packets ->
            packets?.let {
                packetList.visibility = View.VISIBLE
                packetAdapter.updatePacketListAdapter(packets)
            }
        })
        viewModel.result.observe(viewLifecycleOwner, Observer {result ->
            result?.let {
                packetTitle.text = result.PacketTitle
                packetSubTitle.text = result.PacketSubTitle
            }
        })
        viewModel.packet.observe(viewLifecycleOwner, Observer {packet ->
            packet?.let {
                errorMessage.text = packet.ErrorMessage
                isSuccess = packet.Success!!
            }

        })
    }


}
