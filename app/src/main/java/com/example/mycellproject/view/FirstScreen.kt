package com.example.mycellproject.view

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.example.mycellproject.Adapter.UserModuleListAdapter
import com.example.mycellproject.R
import com.example.mycellproject.util.downloadFromUrl
import com.example.mycellproject.util.placeholderProgressBar
import com.example.mycellproject.viewmodel.FirstScreenViewModel
import kotlinx.android.synthetic.main.fragment_first_screen.*


class FirstScreen : Fragment() {
    //private lateinit var viewPager : ViewPager
    private lateinit var viewModel : FirstScreenViewModel
    private var userModuleAdapter = UserModuleListAdapter(arrayListOf())
    var Success: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FirstScreenViewModel::class.java)
        viewModel.getMainData()

        btn_TlYukle.setOnClickListener {
            val action =
                FirstScreenDirections.actionFirstScreenToSecondScreen()
            Navigation.findNavController(it).navigate(action)
        }
        moduleList.layoutManager = LinearLayoutManager(context)
        moduleList.adapter = userModuleAdapter

        observeLiveData()
    }
    fun observeLiveData() {
        viewModel.module.observe(viewLifecycleOwner, Observer {module ->
            module.let {
                moduleList.visibility = View.VISIBLE
                userModuleAdapter.updateUserModuleListAdapter(module)
            }
        })
         viewModel.mainResponse.observe(viewLifecycleOwner, Observer {mainResponse ->
             mainResponse?.let {
                 errorMessage.text = mainResponse.ErrorMessage
                 Success = mainResponse.Success!!
             }
        })
        viewModel.userInfo.observe(viewLifecycleOwner, Observer {userInfo ->
            userInfo?.let {
                userId.text = userInfo.UserId
                userName.text = userInfo.UserName
                userUrl.downloadFromUrl(userInfo.UserImageUrl, placeholderProgressBar(requireView().context))
            }
        })
    }
}
