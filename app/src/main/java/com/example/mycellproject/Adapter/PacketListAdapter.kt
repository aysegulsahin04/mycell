package com.example.mycellproject.Adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mycellproject.PacketList
import com.example.mycellproject.R
import com.example.mycellproject.Packets
import kotlinx.android.synthetic.main.packet_list.view.*

class PacketListAdapter(val packetList: ArrayList<PacketList>): RecyclerView.Adapter<PacketListAdapter.PacketListViewHolder>(){
    class PacketListViewHolder(var view: View): RecyclerView.ViewHolder(view) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PacketListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.packet_list,parent,false)
        return PacketListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return packetList.size
    }

    override fun onBindViewHolder(holder: PacketListViewHolder, position: Int) {
        holder.view.title.text = packetList[position].Title
        holder.view.appSize.text = packetList[position].AppSize.toString()
        holder.view.gb.text = packetList[position].Gb.toString()
        holder.view.dk.text = packetList[position].Dk.toString()
        holder.view.sms.text = packetList[position].Sms.toString()
        holder.view.mins.text = packetList[position].Mins.toString()
        holder.view.price.text = packetList[position].Price.toString()
        holder.view.currency.text = packetList[position].Currency
    }

    fun updatePacketListAdapter(newPacketList: List<PacketList>){
        packetList.clear()
        packetList.addAll(newPacketList)
        notifyDataSetChanged()
    }



}

