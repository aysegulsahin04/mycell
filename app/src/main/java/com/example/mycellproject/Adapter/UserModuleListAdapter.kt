package com.example.mycellproject.Adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mycellproject.R
import com.example.mycellproject.model.MainResponse
import com.example.mycellproject.model.Module
import kotlinx.android.synthetic.main.user_module_list.view.*

class UserModuleListAdapter(val moduleList: ArrayList<Module>): RecyclerView.Adapter<UserModuleListAdapter.UserModuleListViewHolder>(){
    class UserModuleListViewHolder(var view: View): RecyclerView.ViewHolder(view) {

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserModuleListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.user_module_list,parent,false)
        return UserModuleListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return moduleList.size
    }

    override fun onBindViewHolder(holder: UserModuleListViewHolder, position: Int) {
        holder.view.moduleName.text = moduleList[position].ModuleName
    }

    fun updateUserModuleListAdapter(newUserModuleList: List<Module>){
        moduleList.clear()
        moduleList.addAll(newUserModuleList)
        notifyDataSetChanged()
    }




}